import argparse
import numpy as np
import os
import pickle
import torch
import torchvision
import torchvision.transforms as transforms


class ConvNet(torch.nn.Module):
    def __init__(self):
        super(ConvNet, self).__init__()
        self.conv = torch.nn.Sequential(
            torch.nn.Conv2d(3, 32, 3, padding=1),
            torch.nn.ReLU(),
            torch.nn.BatchNorm2d(32),
            torch.nn.Conv2d(32, 32, 3, padding=1),
            torch.nn.ReLU(),
            torch.nn.BatchNorm2d(32),
            torch.nn.MaxPool2d(3, padding=1),
            torch.nn.Dropout2d(0.2),
            torch.nn.Conv2d(32, 64, 3, padding=1),
            torch.nn.ReLU(),
            torch.nn.BatchNorm2d(64),
            torch.nn.Conv2d(64, 64, 3, padding=1),
            torch.nn.ReLU(),
            torch.nn.BatchNorm2d(64),
            torch.nn.MaxPool2d(3, padding=1),
            torch.nn.Dropout2d(0.3),
            torch.nn.Conv2d(64, 128, 3, padding=1),
            torch.nn.ReLU(),
            torch.nn.BatchNorm2d(128),
            torch.nn.Conv2d(128, 128, 3, padding=1),
            torch.nn.ReLU(),
            torch.nn.BatchNorm2d(128),
            torch.nn.MaxPool2d(3, padding=1),
            torch.nn.Dropout2d(0.4),
        )
        self.linear = torch.nn.Sequential(
            torch.nn.Linear(128 * 2 * 2, 10),
            torch.nn.Softmax(0)
        )

    def forward(self, x):
        features = self.conv(x)
        features = features.reshape((features.shape[0], -1))
        y_pred = self.linear(features)
        return y_pred


def test(testloader, model):
    model.eval()
    correct_count = 0
    total_count = 0
    with torch.no_grad():
        for batch_idx, (images, labels) in enumerate(testloader):
            images = torch.Tensor(images).to(device)
            outputs = model(images)
            _, prediction = torch.max(outputs, 1)
            correct_count += (prediction == labels).sum()
            total_count += np.shape(images)[0]
    return correct_count / total_count


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--name', type=str, required=True)
    parser.add_argument('--cifar10-root', type=str, required=True)
    parser.add_argument('--batch-size', type=int, default=16)
    parser.add_argument('--num-workers', type=int, default=2)
    parser.add_argument('--no-gpu', action='store_true')
    parser.add_argument('--num-epochs', type=int, default=30)
    parser.add_argument('--export-dir', type=str, required=True)
    parser.add_argument('--checkpoint-iter-step', type=int, default=2)
    parser.add_argument('--load-from-epoch', type=int, default=-1)
    parser.add_argument('--lr', type=float, default=0.01)
    parser.add_argument('--lr-step-size', type=int, default=20)
    parser.add_argument('--lr-decay', type=float, default=0.1)
    options = parser.parse_args()

    device = torch.device('cuda' if torch.cuda.is_available() and not options.no_gpu else 'cpu')

    transform = transforms.Compose(
        [transforms.RandomHorizontalFlip(),
         transforms.RandomCrop(30),
         transforms.ToTensor(),
         transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))]
    )
    trainset = torchvision.datasets.CIFAR10(root=options.cifar10_root, train=True, download=True, transform=transform)
    trainloader = torch.utils.data.DataLoader(trainset, batch_size=options.batch_size, shuffle=True,
                                              num_workers=options.num_workers)

    testset = torchvision.datasets.CIFAR10(root=options.cifar10_root, train=False, download=True, transform=transform)
    testloader = torch.utils.data.DataLoader(trainset, batch_size=options.batch_size, shuffle=True,
                                             num_workers=options.num_workers)
    classes = ('plane', 'car', 'bird', 'cat', 'deer', 'dog', 'frog', 'horse', 'ship', 'truck')

    if options.load_from_epoch >= 0:
        lr = options.lr * (options.lr_decay ** (options.load_from_epoch // options.lr_step_size))
        with open(os.path.join(options.export_dir, "model_epoch{}.p".format(options.load_from_epoch)), 'rb') as file:
            model, accuracy_list = pickle.load(file)
    else:
        lr = options.lr
        model = ConvNet()
        accuracy_list = []
    loss_func = torch.nn.CrossEntropyLoss()
    optimizer = torch.optim.Adam(model.parameters(), lr=lr)
    lr_scheduler = torch.optim.lr_scheduler.StepLR(optimizer, options.lr_step_size, options.lr_decay, -1)
    model.to(device)
    for epoch in range(max(options.load_from_epoch, 0), options.num_epochs):
        model.train()
        epoch_loss = 0
        for batch_idx, (images, labels) in enumerate(trainloader):
            images = torch.Tensor(images).to(device)
            optimizer.zero_grad()
            outputs = model(images)
            loss = loss_func(outputs, labels)
            loss.backward()
            optimizer.step()
            lr_scheduler.step()
            epoch_loss += loss
        epoch_accuracy = test(testloader, model)
        accuracy_list.append(epoch_accuracy)
        print("epoch: {},\taccuracy: {}".format(epoch, epoch_accuracy))
        if epoch % options.checkpoint_iter_step == options.checkpoint_iter_step - 1:
            with open(os.path.join(options.export_dir, "model_epoch{}.p".format(epoch)), 'wb') as file:
                pickle.dump((model, accuracy_list), file)
